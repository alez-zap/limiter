// �������� ������ 127.0.0.1:1700
var http = require('http');
var server = http.createServer();
server.listen(1700, '127.0.0.1');

// �������� ������ ������ �������� �������
var Limiter = require('./limiter');
var limiter = new Limiter();

server.on('request', function(req, res) {
    // ������� favicon ������� � ������� 
    if(req.url === '/favicon.ico') return;

    var message = '';
    try {
        // ������� ����� ������ � �������
        var count = limiter.newRequest();

        // ���� newRequest() �� ������ ����������, ��
        // ��������� ����������� ���������� � ����������
        // �������� ��� ������������
        message = 'HARD SCRIPT! WOW WOW!!!\n';
        message += 'Requests for last '
            + limiter.getTime() + ' ms: '
            + count;
    } catch(e) {
        // ���� newRequest() ������ ����������, ��
        // ������� ������������, ��� ������ �������
        // �������� � �� ����� ��������� ����������
        message = e.message;
    }
    // ���������� ����� ������������
    res.end(message);
});

console.log('Server is running');