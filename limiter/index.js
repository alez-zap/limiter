var params = require('./params');

// ����� �������� �������
function Limiter() {
    this.requests = []; // ������� �������� [�������� = ����� �������]
}

// ��������� ������� � �������
Limiter.prototype.newRequest = function() {
    // �������, ���� �������� ������� �������� 
    if(!params['enable']) return undefined;

    // ���� ������� ������ ��� ������, �� ������ ����������
    if(this.getRequestsCount() >= params['maxReq']) throw Error('Server is too busy');

    // ���� ������� >= params['maxReq'], �� ������� ������ ������
    if(this.requests.length >= params['maxReq']) this.requests.shift();

    // � ���������  � ������� ����� ������
    this.requests.push(new Date().getTime());

    // ������ ������� ���������� ��������
    return this.getRequestsCount();
}

// ���������� ���������� �������� �� ��������� params['time'] ��
Limiter.prototype.getRequestsCount = function() {
    var currentTime = new Date().getTime();
    var counter = 0;
    for(var i = 0; i<this.requests.length; ++i) {
        if(currentTime - this.requests[i] < params['time']) ++counter;
    }
    return counter;
}

Limiter.prototype.getTime = function() {
    return params['time'];
}

module.exports = Limiter;